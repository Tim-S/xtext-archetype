#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${basePackage}.scoping


/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html${symbol_pound}scoping
 * on how and when to use it.
 */
class ${dslName}ScopeProvider extends Abstract${dslName}ScopeProvider {

}
